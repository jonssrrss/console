<?
	use Console\Console;

	/**
	 * Для добавления новой команды необходимо добавить новый элемент в массив $comands,
	 * а так-же реализовать и задать метод, который будет выполняться при вызове команды
	 */

	class MyConsole extends Console {
		public $comands = array(
			'hello' => array(
				'info' => 'Сказать привет',
				'function' => 'getHello'
			),
			'goodbye' => array(
				'info' => 'Сказать пока',
				'function' => 'getGoodbye'
			),
			'info' => array(
				'info' => 'Информировать об аргументах и опциях, которые описаны в команде',
				'function' => 'getInfoCommand'
			),
		);

		public function getHello($arg, $opt) {
			var_export($arg);
			return PHP_EOL.'Привет! Вот мои аргументы.';
		}

		public function getGoodbye($arg, $opt) {
			var_export($opt);
			return PHP_EOL.'Вот мои опции. Пока!';
		}

		public function getInfoCommand($arg, $opt) {
			return $this->getHelpInfo();
		}
	}