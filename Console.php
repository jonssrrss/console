<?
	namespace Console;

	class Console {

		public $comands = array();
		
		function __construct() {}

		/**
		 * Метод для распарсивания команды
		 * 
		 * $data - весь массив с названием команды, файла, аргументами и опциями
		 */
		public function parseComand($data) {
			unset($data[0]);
			$parsing = array(
				'name' => @$data[1],
				'arg' => array(),
				'opt' => array(),
			);
			if (count($data) > 1) {
				foreach ($data as $key => $value) {
					if (preg_match("/\[(.+)\]/", $value)) {
						$parsing['opt'] = $this->getOpts($parsing['opt'], $value);
					} elseif (preg_match("/\{(.+)\}/", $value)) {
						$parsing['arg'] = $this->getArgs($parsing['arg'], $value);
					}
				}
				return $parsing;
			} else {
				return $this->getHelpInfo();
			}
		}

		/**
		 * Метод для распарсивания аргументов
		 * 
		 * $parsing - весь массив с аргументами
		 * $arg - аргумент со скобками
		 */
		public function getArgs($parsing, $arg) {
			$arg = str_replace(['{', '}'], '', $arg);
			if (strripos($arg, ',')) {
				$arg = explode(',', $arg);
			}
			if (is_array($arg)) {
				foreach ($arg as $key => $value) {
					$parsing[] = $value;
				}
			} else {
				$parsing[] = $arg;
			}
			return $parsing;
		}

		/**
		 * Метод для распарсивания опций
		 * 
		 * $parsing - весь массив с опциями
		 * $opt - опции с квадратными скобками
		 */
		public function getOpts($parsing, $opt) {
			$opt = str_replace(['[', ']'], '', $opt);
			$parsing[explode('=', $opt)[0]] = explode('=', $opt)[1];
			if (preg_match("/\{(.+)\}/", $parsing[array_key_last($parsing)])) {
				$parsing[array_key_last($parsing)] = str_replace(['{', '}'], '', $parsing[array_key_last($parsing)]);
				if (strripos($parsing[array_key_last($parsing)], ',')) {
					$parsing[array_key_last($parsing)] = explode(',', $parsing[array_key_last($parsing)]);
				}
			}
			return $parsing;
		}

		public function getHelpInfo() {
			$str = '';
			foreach ($this->comands as $key => $value) {
				$str .= $key . ' - ' . $value['info'].PHP_EOL;
			}
			return $str;
		}


		/**
		 * Метод для вызова команд наследуемого класса
		 * 
		 * $data - распарсенный массив с названием команды, аргументами и опциями
		 */
		public function execute($data) {
			if (isset($data['name'])) {
				if (isset($this->comands[$data['name']])) {
					$res = 'return $this->'.$this->comands[$data['name']]['function'].'($data["arg"], $data["opt"]);';
					return eval($res);
				} else {
					return PHP_EOL.'Данная команда "'.$data['name'].'" не найдена'.PHP_EOL.PHP_EOL.$this->getHelpInfo();
				}
			} else {
				return $this->getHelpInfo();
			}
		}
	}